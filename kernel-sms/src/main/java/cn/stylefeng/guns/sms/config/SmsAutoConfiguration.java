package cn.stylefeng.guns.sms.config;

import cn.stylefeng.guns.sms.config.properties.AliyunSmsProperties;
import cn.stylefeng.guns.sms.core.cache.impl.MapSignManager;
import cn.stylefeng.guns.sms.core.db.SmsInfoInitizlizer;
import cn.stylefeng.guns.sms.core.sms.SmsManager;
import cn.stylefeng.guns.sms.core.sms.service.AliyunSmsManager;
import cn.stylefeng.guns.sms.modular.executor.SmsExecutorImpl;
import cn.stylefeng.guns.sms.modular.service.SmsInfoService;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * aliyun短信发送的配置
 *
 * @author fengshuonan
 * @Date 2018/7/6 下午3:24
 */
@Configuration
public class SmsAutoConfiguration {

    /**
     * 缓存的管理
     *
     * @author fengshuonan
     * @Date 2018/9/21 上午10:59
     */
    @Bean
    public MapSignManager mapSignManager() {
        return new MapSignManager();
    }

    /**
     * 阿里云短信配置
     *
     * @author fengshuonan
     * @Date 2019/12/30 23:06
     */
    @Bean
    @ConfigurationProperties(prefix = "aliyun.sms")
    public AliyunSmsProperties aliyunSmsProperties() {
        return new AliyunSmsProperties();
    }

    /**
     * 短信发送器
     *
     * @author fengshuonan
     * @Date 2019/12/30 23:06
     */
    @Bean
    public SmsExecutorImpl smsServiceProvider() {
        return new SmsExecutorImpl();
    }

    /**
     * 短信信息
     *
     * @author fengshuonan
     * @Date 2019/12/30 23:07
     */
    @Bean
    public SmsInfoService smsInfoService() {
        return new SmsInfoService();
    }

    /**
     * 短信发送方式
     *
     * @author fengshuonan
     * @Date 2019/12/30 23:07
     */
    @Bean
    public SmsManager smsManager() {
        return new AliyunSmsManager();
    }

    /**
     * 验证码表的初始化
     *
     * @author fengshuonan
     * @Date 2019/12/30 23:07
     */
    @Bean
    public SmsInfoInitizlizer smsInfoInitizlizer() {
        return new SmsInfoInitizlizer();
    }

}